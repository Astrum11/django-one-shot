from todos.models import TodoList, TodoItem
from django.forms import ModelForm, ModelChoiceField

class TodoForm(ModelForm):
    class Meta:
        model = TodoList
        fields = (
            "name",
        )

class TodoEditTask(ModelForm):
    list = ModelChoiceField(queryset=TodoList.objects.all(), empty_label=None)
    class Meta:
        model = TodoItem
        fields = (
            "task",
            "due_date",
            "is_completed",
            "list",
        )
