from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoEditTask
# Create your views here.
def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todos": todos
    }
    return render(request, "lists/list.html", context)

def todo_list_detail(request, id):
    print(f"To-do list ID: {id}")
    todo_detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object": todo_detail
    }
    return render(request,"lists/detail.html", context)

def create_todo_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            new_list = form.save()
            return redirect("todo_list_detail", id=new_list.id)
    else:
        form = TodoForm()

    context = {
        "form": form,
    }
    return render(request, "lists/create.html", context)

def edit_todo_list(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo)
        if form.is_valid():
            edited_list = form.save()
            return redirect("todo_list_detail", id= edited_list.id)
    else:
        form = TodoForm(instance=todo)
    context = {
        "form": form,
        "todo_object": todo,
    }
    return render(request, "lists/edit.html", context)

def delete_todo_list(request, id):
    todo = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo.delete()
        return redirect("todo_list_list")
    context = {
        "todo_object":todo
    }
    return render(request, "lists/delete.html", context)

def create_todo_item(request):
    if request.method == "POST":
        form = TodoEditTask(request.POST)
        if form.is_valid():
            new_item = form.save(commit=False)
            new_item.list = form.cleaned_data['list']
            new_item.save()
            return redirect("todo_list_detail", id=new_item.list.id)
    else:
        form = TodoEditTask()
    context = {
        "form": form,
    }
    return render(request, "lists/create_item.html", context)

def update_todo_item(request, item_id):
    todo_item = get_object_or_404(TodoItem, id=item_id)

    if request.method == "POST":
        form = TodoEditTask(request.POST, instance=todo_item)
        if form.is_valid():
            updated_item = form.save()
            return redirect("todo_list_detail", id=updated_item.list.id)
    else:
        form = TodoEditTask(instance=todo_item)

    context = {
        "form": form,
        "todo_item": todo_item,
    }
    return render(request, "lists/update_item.html", context)
