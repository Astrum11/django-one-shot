from django.db import models

class TodoList(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now_add=True)
    #using __str__ method converts objects within this class to a string
    def __str__(self):
        return self.name

# Create your models here.
class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateField(blank=True, null=True )
    is_completed = models.BooleanField(default=False)
    list = models.ForeignKey(
        TodoList,
        related_name='items',
        on_delete=models.CASCADE
    )
